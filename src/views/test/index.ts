import { router } from '@/plugins';
import { routes } from './router';

routes.forEach((route) => router.addRoute(route));
