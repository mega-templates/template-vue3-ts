import type { RouteRecordRaw } from "vue-router";

export const routes: RouteRecordRaw[] = [
  {
    path: '/test/:someParam',
    name: 'test',
    component: async () => import('./../pages/TestPage.vue')
  }
]