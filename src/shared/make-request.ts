export interface MakeRequstReponse<T> {
  response?: T;
  error?: unknown;
}

export async function makeRequest<T>(fnPpromise: Promise<T>) {
  try {
    const response = await fnPpromise;

    return { response };
  } catch (error) {
    return { error };
  }
}
