import { io, Socket } from 'socket.io-client';
import { DefaultSocketOnEvents, IOnDefaultSocketListener, SocketEvents } from './socket-client-types';

class SocketClient {
  private socketInstance?: Socket;
  private isFirstConnectDone: boolean = false; //Прошло ли уже ПЕРВОЕ соединение
  private onDefaultSocketListeners: IOnDefaultSocketListener[] = []; //Список кастомных обработчиков разых ситуаций

  init(host: string) {
    if (this.socketInstance) {
      this.socketInstance.disconnect();
    }

    this.socketInstance = io(host, { forceNew: true, reconnection: true });
    this.socketInstance.on(DefaultSocketOnEvents.CONNECT, () => {
      console.log('SOCKET EVENT: connect');

      this.onDefaultSocketListeners.forEach((el) => {
        if (el.event === DefaultSocketOnEvents.CONNECT && el.onConnect) {
          el.onConnect();
        }
      });
    });
    this.socketInstance.on(DefaultSocketOnEvents.DISCONNECT, (reason: Socket.DisconnectReason) => {
      console.log('SOCKET EVENT: disconnect ', reason);
      this.onDefaultSocketListeners.forEach((el) => {
        if (el.event === DefaultSocketOnEvents.DISCONNECT && el.onDisconnect) {
          el.onDisconnect(reason);
        }
      });
    });
    this.socketInstance.on(DefaultSocketOnEvents.CONNECT_ERROR, (err: Error) => {
      console.log('SOCKET EVENT: connect_error ', err);
      this.onDefaultSocketListeners.forEach((el) => {
        if (el.event === DefaultSocketOnEvents.CONNECT_ERROR && el.onConnectError) {
          el.onConnectError(err);
        }
      });
    });
  }

  //Открываем коннект к сокету
  connect() {
    if (this.isFirstConnectDone) return;
    this.socketInstance?.connect();
    this.isFirstConnectDone = true;
  }

  //Добавление метода на прослушку эвентов ДЕФОЛТНЫХ в массив
  addOnDefaultSocketListener(listener: IOnDefaultSocketListener) {
    this.onDefaultSocketListeners.push(listener);
    return this;
  }

  //Добавление прослушки на кастомные события

  //Откидываем событие
  emit(event: SocketEvents, ...args: any[]) {
    this.socketInstance?.emit(event, args);
  }

  io() {
    return this.socketInstance;
  }
}

export const SOCKET = new SocketClient();
