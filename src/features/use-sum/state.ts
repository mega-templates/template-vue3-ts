import { computed } from 'vue';
import { useSumStore } from './store';

interface IUseSumOptions {
  a: number;
  b: number;
}

export function useSum(opts: IUseSumOptions) {
  // const store = useSumStore();
  // Для работы со стором получаем переменные выше
  const sum = computed(() => opts.a + opts.b);

  return { sum };
}

// Описание входного и выходного интерфейса фичи (аргументы и выходные значения)
