import { defineStore } from 'pinia';

interface SumStore {
  sum: number;
}

export const useSumStore = defineStore({
  id: 'sum',
  state: (): SumStore => ({
    sum: 0,
  }),
  getters: {
    doubleSum(state): number {
      return state.sum * 2;
    },
  },
  actions: {
    setSum(payload: number): void {
      this.sum = payload;
    },
  },
});
