import { createApp } from 'vue';
import App from './App.vue';
import { pinia, router } from '@/plugins';
import '@/views';

console.log('main', router);

createApp(App).use(pinia).use(router).mount('#app');

// router
//   .isReady()
//   .then(() => app.mount('#app'))
//   .catch(() => console.error('Failed to mount application - router error'));
