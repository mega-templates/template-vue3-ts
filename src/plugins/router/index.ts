import { createRouter, createWebHistory } from 'vue-router'

export const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: []
})
console.log(router.getRoutes())
