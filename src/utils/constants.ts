const CONSTANTS = {
  SERVER_IP: 'http://localhost:3101',
};

//API пути - для запросов
const API_PATHS = {
  API_AUTH: '/api/auth',
  API_AUTH_TOKEN_CHECK: '/api/auth/token/check',
  API_AUTH_LOGOUT: '/api/auth/logout',

  //Админка
  SA_API_MEAL_TAGS: '/sa/api/meal-tags',
  SA_API_MEAL_TAGS_ID: (id: string) => '/sa/api/meal-tags/' + id,
  SA_API_MEAL_CATEGORIES: '/sa/api/meal-categories',
  SA_API_MEAL_CATEGORIES_ID: (id: string) => '/sa/api/meal-categories/' + id,
  SA_API_COOCKING_ZONES: '/sa/api/coocking-zones',
  SA_API_COOCKING_ZONES_ID: (id: string) => '/sa/api/coocking-zones/' + id,
  SA_API_MEALS: '/sa/api/meals',
  SA_API_MEALS_ID: (id: string) => '/sa/api/meals/' + id,
  SA_API_WORKERS: '/sa/api/workers',
  SA_API_WORKERS_ID: (id: string) => '/sa/api/workers/' + id,
  SA_API_WORKERS_DISMISS_ID: (id: string) => '/sa/api/workers/' + id + '/dismiss',

  //Пути с заказами
  API_ORDERS: '/api/orders',
  API_ORDERS_ID: (id: string) => '/api/orders/' + id,
  API_ORDERS_ID_MEALS_ADD: (id: string) => '/api/orders/' + id + '/meals/add',
  API_ORDERS_ID_PRED_CHECK: (id: string) => '/api/orders/' + id + '/status/pred-check',
  API_ORDERS_ID_CLOSED: (id: string) => '/api/orders/' + id + '/status/closed',

  //Чет там ...
  API_MEALS_CATEGORIES: '/api/meals-categories',
  API_COOCKING_ZONES: '/api/coocking-zones',
  API_COOCKING_ZONE_ID_ORDERS: (id: string) => '/api/coocking-zones/' + id + '/orders',
  API_COOCKING_ZONE_ZONE_ID_ORDERS_ORDER_ID_READY: (zoneId: string, orderId: string) => '/api/coocking-zones/' + zoneId + '/orders/' + orderId + '/ready',
  API_COOCKING_ZONE_ZONE_ID_ORDERS_ORDER_ID_MEALS_MEAL_ID_READY: (zoneId: string, orderId: string, orderMealId: string) =>
    '/api/coocking-zones/' + zoneId + '/orders/' + orderId + '/meals/' + orderMealId + '/ready',
};

export { API_PATHS, CONSTANTS };
